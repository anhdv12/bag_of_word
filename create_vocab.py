import os, shutil
from glob import glob

def get_label(path:str)->list:
    return path.split('_')[1]

def create_bag(folder:str)->list:
    """ tạo vocab chứa tất cả các từ có thể xảy ra của 1 class """

    bag = []
    for f in os.listdir(folder):
        label = get_label(f)
        words = label.split(' ')
        
        for word in words:
            word = word.upper()
            if word not in bag: 
                bag.append(word)
    
    return bag

fold_DonThuoc = r'/home/scratch/Documents/classify_formdata/classify_string/data/DonThuoc'
fold_GiayKhaiSinh = r'/home/scratch/Documents/classify_formdata/classify_string/data/GiayKhaiSinh'
fold_SoHoKhau = r'/home/scratch/Documents/classify_formdata/classify_string/data/SoHoKhau'


bag_DonThuoc = create_bag(fold_DonThuoc)
bag_GiayKhaiSinh = create_bag(fold_GiayKhaiSinh)
bag_SoHoKhau = create_bag(fold_SoHoKhau)

with open('/home/scratch/Documents/classify_formdata/classify_string/data/Donthuoc.txt', 'w') as f:
    for word in bag_DonThuoc:
        f.write(word + '\n')

with open('/home/scratch/Documents/classify_formdata/classify_string/data/GiayKhaiSinh.txt', 'w') as f:
    for word in bag_GiayKhaiSinh:
        f.write(word + '\n')

with open('/home/scratch/Documents/classify_formdata/classify_string/data/SoHoKhau.txt', 'w') as f:
    for word in bag_SoHoKhau:
        f.write(word + '\n')



# tạo từ điển của cả dataset
abc = []
for word in bag_SoHoKhau + bag_DonThuoc + bag_GiayKhaiSinh:
    word = word.upper()
    if word not in abc:
        abc.append(word)

with open('/home/scratch/Documents/classify_formdata/classify_string/data/vocab_of_dataset.txt', 'w') as f:
    for word in abc:
        f.write(word + '\n')


print(len(abc))
print(len(bag_SoHoKhau))
print(len(bag_DonThuoc))
print(len(bag_GiayKhaiSinh))