import os, shutil
from glob import glob
from unidecode import unidecode


class BagOfWord():
    def __init__(self) -> None:
        
        
        self.classes_path = {
            # class_name    ,   path_vocab
            'donthuoc'      : './data/Donthuoc.txt',
            'giaykhaisinh'  : './data/GiayKhaiSinh.txt',
            'sohokhau'      :'./data/SoHoKhau.txt'
        }

        # tiếng việt có dấu
        self.dictionary = {}
        with open( self.classes_path['donthuoc'] , 'r') as f:
            self.dictionary['donthuoc'] = f.read().strip('\n').splitlines()

        with open( self.classes_path['giaykhaisinh'], 'r') as f:
            self.dictionary['giaykhaisinh'] = f.read().strip('\n').splitlines()

        with open( self.classes_path['sohokhau'], 'r') as f:
            self.dictionary['sohokhau'] = f.read().strip('\n').splitlines()

        #tiếng việt ko dấu
        self.dictionary_unidecode = {
            'donthuoc'      : list( set( [ unidecode( w) for w in self.dictionary[ 'donthuoc']] )) ,
            'giaykhaisinh'  : list( set( [ unidecode( w) for w in self.dictionary[ 'giaykhaisinh']] )),
            'sohokhau'      : list( set( [ unidecode( w) for w in self.dictionary[ 'sohokhau']] )),
        }

        # for k, v in self.dictionary_unidecode.items():
        #     print(k, v)
        # print('\n')


    def word_vector( self, dictionary, seq_label:str, class_name) ->int:
        r""" seq_label là chuỗi các từ ngăn cách bởi 1 dấu space
        ví dụ : Giấy Khai Sinh
                                                         """
        
        words = seq_label.strip().split(' ')
        count = 0
        
        for word in words:
            if word in dictionary[ class_name]:
                count +=1
        return count

    def _predict( self, dictionary, seq_label:str):
        seq_label = seq_label.upper()
        # print(seq_label)
        class_dict = {}
        class_dict['donthuoc']      = self.word_vector( dictionary, seq_label, 'donthuoc')
        class_dict['giaykhaisinh'] = self.word_vector( dictionary, seq_label, 'giaykhaisinh')
        class_dict['sohokhau']  = self.word_vector( dictionary, seq_label, 'sohokhau')

        max_value = -1
        list_class = []
        for value in class_dict.values():
            if value > max_value:
                max_value = value
        
        for key, value in class_dict.items():
            if value == max_value:
                list_class.append(key)

        return list_class


    def predict(self, seq_label:str):
        list_class = self._predict( self.dictionary, seq_label )

        if len(list_class) > 1:
            seq_label = unidecode(seq_label)
            list_class = self._predict( self.dictionary_unidecode, seq_label)
        
        return list_class
